#!/bin/bash

#sed "s/port: .*$/port: $PROXY_PORT/" -i /etc/squid/squid.conf
cat /etc/squid/squid.conf
#/etc/init.d/squid restart 
nohup echo "${OPENCONNECT_PASSWORD}" | openconnect -u ${OPENCONNECT_USER} --passwd-on-stdin ${OPENCONNECT_CERT:+--servercert} ${OPENCONNECT_CERT:---no-cert-check} ${OPENCONNECT_URL} &
#date
while $(ip link show tun0 > /dev/null && true ) ;do echo "Wainting for tun0 ... " ; sleep 1 ;done
/etc/init.d/squid restart 

######### Route Requests
if [ ${ROUTE_ENABLE} == 'Y' ] 
then iptables -t nat -A POSTROUTING -o tun0 -j MASQUERADE
iptables -A FORWARD -i tun0 -o eth0 -m state --state RELATED,ESTABLISHED -j ACCEPT
iptables -A FORWARD -i eth0 -o tun0 -j ACCEPT
ip route  
fi

wait

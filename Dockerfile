FROM ubuntu:18.04
#FROM debian:stable

MAINTAINER Shabahang Elmian <shabahang@gmail.com>

RUN apt-get update && apt-get install -y openconnect squid3 iptables
RUN apt-get install -y curl wget vim
ADD squid.conf /etc/squid/squid.conf
ADD entrypoint.sh /entrypoint.sh
RUN chmod +x /entrypoint.sh
HEALTHCHECK --interval=30s --timeout=25s \
  CMD export http_proxy=http://user1:user1@127.0.0.1:3128 && curl -sf http://www.facebook.com ||  /etc/init.d/squid restart
ENTRYPOINT /entrypoint.sh
